<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>LaraBlog</title>
</head>

<body>
    <header class="bg-slate-900 p-5 flex justify-between items-center">
        <div>
            <a href="" class="logo text-2xl text-orange-600 font-bold">LaraBlog</a>
        </div>

        <div class="text-lg font-semibold max-md:hidden">
            <a class="link-nav ml-2 mr-2 text-white hover:text-orange-600" href="/">Accueil</a>
            <a class="link-nav ml-2 mr-2 text-white hover:text-orange-600" href="/publier-un-article">Publier un article</a>
            <a class="link-nav ml-2 mr-2 text-white hover:text-orange-600" href="">Profile</a>
            <a class="link-nav ml-2 mr-2 text-orange-600 p-2 hover:bg-slate-900 bg-white rounded-lg border border-white"
                href="">Se déconnecter</a>
            @auth
                <a class="link-nav ml-2 mr-2 text-white hover:text-orange-600" href="">Se connecter</a>
                <a class="link-nav ml-2 mr-2 text-white hover:text-orange-600" href="">S'inscrire</a>
            @endauth
        </div>
    </header>
    <section>
        @yield('page-content')
    </section>
    <footer class="bg-slate-900 p-4 flex justify-center">
        <span class="text-white footer-text hover:text-orange-600 cursor-pointer">LaraBlog Copyright © 2023</span>
    </footer>

</body>

</html>
