@extends('./layout/layout')
@section('page-content')
<div class="flex justify-center">
<form action="/publier-un-article" method="post">
    @if ($errors)
        @foreach ($errors->all() as $error)
            <ul>
                <li>{{$error}}</li>
            </ul>
        @endforeach
    @endif
    @method('post')
    @csrf
    <div class="flex m-2">
    <input class="border-orange-600 border w-96 h-12 outline-none rounded-lg" type="text" placeholder="Nom" name="nom" value="{{old('nom')}}">
    </div>
<div class="flex m-2">
    <input class="border-orange-600 border w-96 h-12 outline-none rounded-lg" type="email" placeholder="Email" name="email" value="{{old('email')}}">
    </div>
<div class="flex justify-center m-2">
    <input class="border-orange-600 border h-12 w-40 outline-none rounded-lg button font-bold text-slate-900" type="submit" value="Valider">
    </div>
</form>
</div>
@endsection
