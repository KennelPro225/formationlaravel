<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateFormRequest;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function Article()
    {
        return view("article");
    }

    public function Store(ValidateFormRequest $request)
    {
        $validation = $request;
        if ($validation) {
            return $validation;
        } else {
            return redirect()->back();
        }
    }
}
