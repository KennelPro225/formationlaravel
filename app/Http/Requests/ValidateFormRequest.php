<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "nom" => "required|min: 2|string",
            "email" => "required|email"];
    }
    public function messages()
    {
        return [
            'nom.required' => "Le champ Nom est requis",
            "nom.min"=> "Le champ Nom doit contenir au minimum 2 caractères ",
            "email.required" => "Le champ Email est requis",
            "email.email" => "L'email entré est incorrect",
        ];
    }
}
